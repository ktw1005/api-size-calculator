package config

import (
	"net/url"
	"strings"

	"github.com/urfave/cli"
)

const ApiUri = "/api/v1"   // REST API
const ApiUriV2 = "/api/v2" // REST API

type Config struct {
	options *Options
}

// NewConfig initialises a new configuration file
func NewConfig(ctx *cli.Context) (*Config, error) {
	options, err := NewOptions(ctx)
	if err != nil {
		return nil, err
	}
	c := &Config{
		options: options,
	}
	return c, nil
}

// BaseUri returns the site base URI for a given resource.
func (c *Config) BaseUri(res string) string {
	if c.SiteUrl() == "" {
		return res
	}

	u, err := url.Parse(c.SiteUrl())

	if err != nil {
		return res
	}

	return strings.TrimRight(u.EscapedPath(), "/") + res
}

// ApiUri returns the api URI.
func (c *Config) ApiUri() string {
	return c.BaseUri(ApiUri)
}

// SiteUrl returns the public server URL (default is "http://localhost:8080/").
func (c *Config) SiteUrl() string {
	if c.options.SiteUrl == "" {
		return "127.0.0.1:8080/"
	}

	return strings.TrimRight(c.options.SiteUrl, "/") + "/"
}

// HttpMode returns the server mode.
func (c *Config) HttpMode() string {
	if c.options.HttpMode == "" {
		return "release"
	}

	return c.options.HttpMode
}

// HttpHost returns the built-in HTTP server host name or IP address (empty for all interfaces).
func (c *Config) HttpHost() string {
	if c.options.HttpHost == "" {
		return "0.0.0.0"
	}

	return c.options.HttpHost
}

// HttpPort returns the built-in HTTP server port.
func (c *Config) HttpPort() int {
	if c.options.HttpPort == 0 {
		return 8080
	}

	return c.options.HttpPort
}
