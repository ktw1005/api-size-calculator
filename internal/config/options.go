package config

import (
	"strconv"

	"github.com/urfave/cli"
)

type Options struct {
	SiteUrl  string
	HttpHost string
	HttpPort int
	HttpMode string
}

func NewOptions(ctx *cli.Context) (*Options, error) {
	c := &Options{}

	if ctx == nil {
		return c, nil
	}

	if err := c.SetOptions(ctx); err != nil {
		return c, err
	}

	return c, nil
}

func (c *Options) SetOptions(ctx *cli.Context) error {
	flags := ctx.App.Flags
	for _, e := range flags {
		switch e.GetName() {
		case "siteurl":
			c.SiteUrl = e.(*cli.StringFlag).GetValue()
		case "httphost":
			c.HttpHost = e.(*cli.StringFlag).GetValue()
		case "httpport":
			port, err := strconv.Atoi((e.(*cli.IntFlag).GetValue()))
			if err != nil {
				return err
			}
			c.HttpPort = port
		case "httpmode":
			c.HttpMode = e.(*cli.StringFlag).GetValue()
		}
	}
	return nil
}
