package server

import (
	"net/http"
	apiv1 "sizecalculator/internal/api/v1"
	apiv2 "sizecalculator/internal/api/v2"
	"sizecalculator/internal/calculate"
	"sizecalculator/internal/config"

	"github.com/gin-gonic/gin"
)

func registerRoutes(router *gin.Engine, conf *config.Config, resource *calculate.WebResource) {
	router.RedirectTrailingSlash = true

	//health check page
	router.GET(conf.BaseUri("/"), func(c *gin.Context) {
		c.String(http.StatusOK, "ok")
	})

	// JSON-REST API Version 1
	v1 := router.Group(conf.BaseUri(config.ApiUri))
	// JSON-REST API Version 2
	v2 := router.Group(conf.BaseUri(config.ApiUriV2))

	apiv1.Calculator(v1, resource)
	apiv2.Calculator(v2, resource)

	router.NoRoute()
}
