package server

import (
	"context"
	"fmt"
	"net/http"
	"sizecalculator/internal/calculate"
	"sizecalculator/internal/config"
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func Start(ctx context.Context, conf *config.Config, log *zap.Logger) error {
	start := time.Now()

	gin.SetMode(conf.HttpMode())

	// Create new HTTP router engine without standard middleware.
	router := gin.New()

	// Register logger middleware.
	router.Use(Logger(log))

	webbResource := calculate.NewURLService(http.DefaultClient)
	// Register HTTP route handlers.
	registerRoutes(router, conf, webbResource)

	// Create new HTTP server instance.
	server := &http.Server{
		Addr:    fmt.Sprintf(":%d", conf.HttpPort()),
		Handler: router,
	}

	log.Debug("http: successfully initialized", zap.Duration("time: ", time.Since(start)))

	errs := make(chan error)
	// Start HTTP server.
	go func() {
		log.Info("http: starting web server", zap.String("address", server.Addr))

		if err := server.ListenAndServe(); err != nil {
			if err == http.ErrServerClosed {
				log.Info("http: web server shutdown complete")
			} else {
				log.Error("http: web server closed unexpectedly", zap.Error(err))
				errs <- err
			}
		}
	}()

	defer func() {
		c, cancel := context.WithTimeout(context.Background(), 5)
		defer cancel()
		if err := server.Shutdown(c); err != nil {
			log.Error("graceful shutdown of the internal HTTP server failed", zap.Error(err))
		}
	}()

	// Graceful HTTP server shutdown.
	select {
	case err := <-errs:
		return err
	case <-ctx.Done():
		return nil
	}
}
