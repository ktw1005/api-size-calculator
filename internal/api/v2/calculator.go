package v2

import (
	"github.com/gin-contrib/cache"
	"github.com/gin-contrib/cache/persistence"
	"net/http"
	"sizecalculator/internal/api"
	"sizecalculator/internal/sanitize"
	"sort"
	"time"

	"github.com/gin-gonic/gin"
)

type response struct {
	Size     int    `json:"size"`
	Category string `json:"category"`
}

type calculator interface {
	URL(url string) (*http.Response, int, error)
}

var ranges = []int{-1, 1000, 3000, 5000}
var names = []string{"", "SMALL", "MEDIUM", "LARGE", "X-LARGE", ""}

func Calculator(router *gin.RouterGroup, cal calculator) {
	store := persistence.NewInMemoryStore(time.Second)
	router.GET("/calculator", cache.CachePage(store, time.Minute, func(c *gin.Context) {
		resource := sanitize.URL(c.Query("file"))
		if resource == "" {
			api.Error(c, http.StatusBadRequest, nil, "URL not provided")
			return
		}

		resp, size, err := cal.URL(resource)
		if err != nil {
			api.Error(c, http.StatusInternalServerError, err, "Internal Server Error")
			return
		} else if resp.StatusCode >= 400 {
			api.Error(c, resp.StatusCode, nil, resp.Status)
			return
		}

		response := &response{
			Size:     size,
			Category: getCategory(size),
		}

		c.JSON(resp.StatusCode, response)
	}))
}

func getCategory(n int) string {
	return names[sort.SearchInts(ranges, n)]
}
