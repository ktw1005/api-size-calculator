package api

type Response struct {
	Code    int    `json:"code"`
	Err     string `json:"error,omitempty"`
	Msg     string `json:"message,omitempty"`
	Details string `json:"details,omitempty"`
}

func NewResponse(code int, msg string) Response {
	if code < 400 {
		return Response{Code: code, Msg: msg}
	} else {
		return Response{Code: code, Err: msg}
	}
}
