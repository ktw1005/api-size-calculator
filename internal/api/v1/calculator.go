package v1

import (
	"net/http"
	"sizecalculator/internal/api"
	"sizecalculator/internal/sanitize"
	"time"

	"github.com/gin-contrib/cache"
	"github.com/gin-contrib/cache/persistence"
	"github.com/gin-gonic/gin"
)

type response struct {
	Size int `json:"size"`
}

type calculator interface {
	URL(url string) (*http.Response, int, error)
}

func Calculator(router *gin.RouterGroup, cal calculator) {
	store := persistence.NewInMemoryStore(time.Second)
	router.GET("/calculator", cache.CachePage(store, time.Minute, func(c *gin.Context) {
		resource := sanitize.URL(c.Query("file"))
		if resource == "" {
			api.Error(c, http.StatusBadRequest, nil, "URL not provided")
			return
		}

		resp, size, err := cal.URL(resource)
		if err != nil {
			api.Error(c, http.StatusInternalServerError, err, "Internal Server Error")
			return
		} else if resp.StatusCode >= 400 {
			api.Error(c, resp.StatusCode, nil, resp.Status)
			return
		}

		response := &response{
			Size: size,
		}

		c.JSON(resp.StatusCode, response)
	}))
}
