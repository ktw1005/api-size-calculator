package v1

import (
	"errors"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestCalculator(t *testing.T) {
	type args struct {
		url string
	}
	type fields struct {
		MockExpectations func(c *calculatorMock)
	}
	type want struct {
		Code     int
		Response []byte
	}
	tests := []struct {
		name   string
		args   args
		fields fields
		want   want
	}{
		{
			name: "success",
			args: args{url: "/api/v1/calculator?file=http://a.com/a.jpg"},
			fields: fields{MockExpectations: func(c *calculatorMock) {
				c.OnURL("http://a.com/a.jpg").Return(&http.Response{StatusCode: 200}, 1, nil)
			}},
			want: want{
				Code:     200,
				Response: []byte(`{"size":1}`),
			},
		},
		{
			name:   "empty query string",
			args:   args{url: "/api/v1/calculator"},
			fields: fields{MockExpectations: func(c *calculatorMock) {}},
			want: want{
				Code:     400,
				Response: []byte(`{"code":400,"error":"URL not provided"}`),
			},
		},
		{
			name: "internal error",
			args: args{url: "/api/v1/calculator?file=http://a.com/a.jpg"},
			fields: fields{MockExpectations: func(c *calculatorMock) {
				c.OnURL("http://a.com/a.jpg").Return((*http.Response)(nil), 0, errors.New("fail"))
			}},
			want: want{
				Code:     500,
				Response: []byte(`{"code":500,"error":"Internal Server Error","details":"fail"}`),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var calcMock calculatorMock
			tt.fields.MockExpectations(&calcMock)
			gin.SetMode(gin.TestMode)
			app := gin.New()
			router := app.Group("/api/v1")
			Calculator(router, &calcMock)
			r := PerformRequest(app, "GET", tt.args.url)
			w := httptest.NewRecorder()
			_, err :=w.Write(tt.want.Response)
			require.NoError(t, err, "write error")
			assert.Equal(t, tt.want.Code, r.Code)
			assert.Equal(t, w.Body, r.Body)
		})
	}
}

type calculatorMock struct {
	mock.Mock
}

func (m *calculatorMock) URL(url string) (*http.Response, int, error) {
	args := m.Called(url)
	return args.Get(0).(*http.Response), args.Get(1).(int), args.Error(2)
}

func (m *calculatorMock) OnURL(url string) *mock.Call {
	return m.On("URL", url)
}

// Performs API request with empty request body.
func PerformRequest(r http.Handler, method, path string) *httptest.ResponseRecorder {
	req, _ := http.NewRequest(method, path, nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	return w
}
