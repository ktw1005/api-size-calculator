package api

import (
	"github.com/gin-gonic/gin"
)

func Error(c *gin.Context, code int, err error, msg string) {
	resp := NewResponse(code, msg)

	if err != nil {
		resp.Details = err.Error()
	}

	c.AbortWithStatusJSON(code, resp)
}
