package calculate

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/mock"
)

func TestURL(t *testing.T) {
	type args struct {
		url string
	}
	type fields struct {
		MockExpectations func(c *httpClientMock)
	}
	type want struct {
		Response *http.Response
		Size     int
		Error    error
	}
	tests := []struct {
		name   string
		args   args
		fields fields
		want   want
	}{
		{
			name: "content-length returned size",
			args: args{url: "https://www.sbs.com.au/large-image.jpeg"},
			fields: fields{MockExpectations: func(c *httpClientMock) {
				c.OnHead("https://www.sbs.com.au/large-image.jpeg").Return(&http.Response{ContentLength: 1024}, nil)
			}},
			want: want{
				Response: &http.Response{ContentLength: 1024},
				Size:     1,
				Error:    nil,
			},
		},
		{
			name: "content-length returned -1 and downloaded whole file",
			args: args{url: "https://www.sbs.com.au/large-image.jpeg"},
			fields: fields{MockExpectations: func(c *httpClientMock) {
				c.OnHead("https://www.sbs.com.au/large-image.jpeg").Return(&http.Response{ContentLength: -1}, nil)
				c.OnGet("https://www.sbs.com.au/large-image.jpeg").Return(&http.Response{Body: &ReaderCloser{Data: returnBytes(1024), loc: 0}}, nil)
			}},
			want: want{
				Response: &http.Response{Body: &ReaderCloser{Data: returnBytes(1024), loc: 1024}},
				Size:     1,
				Error:    nil,
			},
		},
		{
			name: "error head request",
			args: args{url: "https://www.sbs.com.au/large-image.jpeg"},
			fields: fields{MockExpectations: func(c *httpClientMock) {
				c.OnHead("https://www.sbs.com.au/large-image.jpeg").Return((*http.Response)(nil), errors.New("test error"))
			}},
			want: want{
				Response: (*http.Response)(nil),
				Size:     0,
				Error:    errors.New("failed fetching resource"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var httpMock httpClientMock
			tt.fields.MockExpectations(&httpMock)

			s := NewURLService(&httpMock)
			result, size, err := s.URL(tt.args.url)

			if tt.want.Error != nil {
				assert.Equal(t, tt.want.Error, err, "error")
			}
			assert.Equal(t, tt.want.Size, size, "size")
			assert.Equal(t, tt.want.Response, result, "result")
			httpMock.AssertExpectations(t)
		})
	}
}

type httpClientMock struct {
	mock.Mock
}

func (m *httpClientMock) Head(url string) (resp *http.Response, err error) {
	args := m.Called(url)
	return args.Get(0).(*http.Response), args.Error(1)
}

func (m *httpClientMock) OnHead(url string) *mock.Call {
	return m.On("Head", url)
}

func (m *httpClientMock) Get(url string) (resp *http.Response, err error) {
	args := m.Called(url)
	return args.Get(0).(*http.Response), args.Error(1)
}

func (m *httpClientMock) OnGet(url string) *mock.Call {
	return m.On("Get", url)
}

type ReaderCloser struct {
	Data []byte
	loc  int
}

func (r *ReaderCloser) Read(p []byte) (n int, err error) {
	if r.loc >= len(r.Data) {
		return 0, io.EOF
	}
	for i := 0; i < len(p)-1; i++ {
		p[i] = r.Data[r.loc+i]
	}
	r.loc = r.loc + len(p)
	return len(p), nil
}

func (r *ReaderCloser) Close() error {
	return nil
}

func returnBytes(n int) []byte {
	var data []byte
	for i := 0; i < n; i++ {
		data = append(data, 65)
	}
	return data
}
