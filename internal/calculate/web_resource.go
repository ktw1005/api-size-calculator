package calculate

import (
	"errors"
	"io"
	"net/http"
)

type HTTPClient interface {
	Get(url string) (resp *http.Response, err error)
	Head(url string) (resp *http.Response, err error)
}

type WebResource struct {
	httpClient HTTPClient
}

func NewURLService(httpClient HTTPClient) *WebResource {
	return &WebResource{
		httpClient: httpClient,
	}
}

func (c *WebResource) URL(url string) (*http.Response, int, error) {
	resp, err := c.httpClient.Head(url)
	if err != nil {
		return nil, 0, errors.New("failed fetching resource")
	}

	if resp.ContentLength > 0 {
		return resp, int(resp.ContentLength) / 1024, nil
	}

	resp, err = c.httpClient.Get(url)
	if err != nil {
		return nil, 0, errors.New("failed fetching resource")
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, 0, errors.New("failed reading response body")
	}

	return resp, len(body) / 1024, nil

}
