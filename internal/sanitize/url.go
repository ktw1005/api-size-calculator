package sanitize

import "regexp"

// URL returns a formatted url friendly string.
func URL(original string) string {
	emptySpace := []byte("")
	urlRegExp := regexp.MustCompile(`[^a-zA-Z0-9-_/:.,?&@=#%]`)
	return string(urlRegExp.ReplaceAll([]byte(original), emptySpace))
}
