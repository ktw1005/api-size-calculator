#!/usr/bin/env bash

EXPECTED_COVERAGE=${EXPECTED_COVERAGE:-80}

cov=`go tool cover -func=coverage.out | tail -n 1 | sed 's/[^0-9\.]*//g'`
# bash only works with integers => covi = math.floor(cov)
covi=$( echo "$cov" | sed 's/\.[0-9]*$//g' )

if [ $covi -lt $EXPECTED_COVERAGE ]
then
    die "ERROR: Test coverage is not enough! Want at least $EXPECTED_COVERAGE% but only $cov% of tested packages are covered with tests."
else
    echo "SUCCESS: Coverage is ~$cov% (minimum expected is $EXPECTED_COVERAGE%)"
fi
