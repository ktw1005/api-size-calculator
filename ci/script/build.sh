#!/usr/bin/env bash

ECR_REPO_NAME=$1

echo $(aws ecr get-login --no-include-email --region ap-southeast-2)  > login.sh
sh login.sh
docker build -t $ECR_REPO_NAME .
docker tag $ECR_REPO_NAME:latest 243747685464.dkr.ecr.ap-southeast-2.amazonaws.com/$ECR_REPO_NAME:latest
docker push 243747685464.dkr.ecr.ap-southeast-2.amazonaws.com/$ECR_REPO_NAME:latest