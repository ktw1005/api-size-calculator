#!/usr/bin/env bash
docker run -t --rm -v $(pwd):/app -w /app golangci/golangci-lint:v1.44.2 golangci-lint run -v