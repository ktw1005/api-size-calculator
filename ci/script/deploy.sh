#!/usr/bin/env bash
STACK_NAME=$1

aws cloudformation deploy --stack-name $STACK_NAME-service --template-file ci/infrastructure/ecs-fargate.yml --parameter-overrides ServiceName=$STACK_NAME-service ImageUrl=243747685464.dkr.ecr.ap-southeast-2.amazonaws.com/$STACK_NAME:latest ContainerPort=8080 DesiredCount=2 StackName=$STACK_NAME