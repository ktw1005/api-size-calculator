REPO_NAME := api-size-calculator
BINARY_NAME := $(REPO_NAME)

run:
	docker-compose up
.DEFAULT:

build:
	go build ./cmd/$(BINARY_NAME)
.PHONY: build

docker-build:
	docker build --tag "api-size-calculator:builder" .
.PHONY: docker-build

test:
	go test ./... -covermode=atomic -coverprofile=coverage.out
.PHONY: test

cover: test
	./ci/script/coverage.sh
.PHONY: cover

check-depend:
	go mod tidy -v
	go mod verify
.PHONY: check-depend

lint:
	./ci/script/lint.sh
.PHONY: lint

fmt:
	go fmt ./...
.PHONY: fmt
