ARG REPO_NAME=api-size-calculator
ARG REPO_PATH=/go/src/bitbucket.org/ktw1005/$REPO_NAME

FROM golang:1.17.8-alpine3.15 AS build
ARG REPO_PATH

RUN apk add --no-cache bash~=5 make~=4 gcc libc-dev

WORKDIR $REPO_PATH

COPY . ./
RUN go mod download
RUN make build

FROM alpine:3.15 AS release
ARG REPO_NAME
ARG REPO_PATH

COPY --from=build $REPO_PATH/$REPO_NAME /go/bin/$REPO_NAME
EXPOSE 8080
ENTRYPOINT /go/bin/api-size-calculator
