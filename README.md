# Size Calculator

- [Size Calculator](#size-calculator)
  - [Overview](#overview)
  - [Getting Started](#getting-started)
    - [Local environment using docker](#local-environment-using-docker)
    - [Local environment](#local-environment)
    - [Try it out on live endpoint](#try-it-out-on-live-endpoint)
  - [Test](#test)
  - [API Documentation](#api-documentation)
    - [Base URL for API Version](#base-url-for-api-version)
    - [GET /file](#get-file)
      - [query parameters](#query-parameters)
      - [Response code](#response-code)
      - [Response example](#response-example)
        - [version 1](#version-1)
        - [version 2](#version-2)
      - [400 response](#400-response)
  - [Developer Note](#developer-note)
    - [Repo](#repo)
    - [Deployed on AWS](#deployed-on-aws)
    - [Techinial note](#techinial-note)
## Overview

The service provides rest api that accepts resource url and returns the size of resource it contains. 

## Getting Started

### Local environment using docker 

- `make` will build docker image and start up the service
- run v1 endpoint `curl -f http://localhost:8080/api/v1/calculator?file=https://rb.gy/9rbfrh` for size only response

```json
{
    "size": 8842
}
```

- run v2 endpoint `curl -f http://localhost:8080/api/v2/calculator?file=https://rb.gy/9rbfrh` for size and category response

```json
{
    "size": 8842,
    "category": "X-LARGE"
}
```

### Local environment 

- `make bulid` to build binary
- `./api-size-calculator` to start up the service
- You can make same `curl` request.
- Note) Stop docker container or vice versa as the service will run on same port 8080

### Try it out on live endpoint

You can click below url to see it working. Try different resource url by replacing the url after `file=`

http://sbs-PublicL-1E2RIC5CUOQ8D-2130531057.ap-southeast-2.elb.amazonaws.com/api/v2/calculator?file=https://rb.gy/9rbfrh

## Test

- `make test` will run the test on packages that contains calculator logic. 
- Configuration and HTTP server starting up part doesn't have test yet.

## API Documentation

### Base URL for API Version

| version   | URL     |
| --------- | ------- |
| Version 1 | /api/v1 |
| Version 2 | /api/v2 |

### GET /file

#### query parameters

| name | value                             | type   | required |
| ---- | --------------------------------- | ------ | -------- |
| file | url link eg) https://rb.gy/9rbfrh | string | required |

#### Response code

| code | description|
|--|--|
|200| successful |
|400| bad user input, missing url|
|500| internal server error|

#### Response example

##### version 1 

```
{
    "size": 8842
}

```

##### version 2

```
{
    "size": 8842,
    "category": "X-LARGE"
}
```

#### 400 response

```
{
    "code": 400,
    "error": "URL not provided"
}
```

## Developer Note

### Repo

Link to bitbucket repo https://bitbucket.org/ktw1005/api-size-calculator

### Deployed on AWS

- Bitbucket pipeline setup with Fargate cloud formation yaml file. [LINK](https://bitbucket.org/ktw1005/api-size-calculator/src/master/bitbucket-pipelines.yml)
- Pipeline run [LINK](https://bitbucket.org/ktw1005/api-size-calculator/addon/pipelines/home#!/results/page/1)


### Techinial note

- It first make `HEAD` requests to fetch `content-length` and use it to determine the size of resource
- If `HEAD` request doesn't contain `content-lenth` then make `GET` requests and read body size.
- uses `cache` based on the url and return cached response

