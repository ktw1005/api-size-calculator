package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"sizecalculator/internal/config"
	"sizecalculator/internal/server"
	"syscall"

	"github.com/urfave/cli"
	"go.uber.org/zap"
)

func main() {
	app := &cli.App{
		Name: "start",
		Flags: []cli.Flag{
			&cli.StringFlag{Name: "siteurl", Value: "http://localhost:8080/", Usage: "language for the greeting", EnvVar: "APP_SITE_URL"},
			&cli.StringFlag{Name: "httphost", Value: "0.0.0.0", Usage: "language for the greeting", EnvVar: "APP_HTTP_HOST"},
			&cli.IntFlag{Name: "httpport", Value: 8080, Usage: "language for the greeting", EnvVar: "APP_HTTP_PORT"},
			&cli.StringFlag{Name: "httpmode", Value: "release", Usage: "language for the greeting", EnvVar: "APP_HTTP_MODE"},
		},
		Action: startAction,
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

}

func startAction(ctx *cli.Context) error {
	conf, err := config.NewConfig(ctx)
	if err != nil {
		return err
	}

	logger, err := zap.NewProduction()
	defer func() {
		if err := logger.Sync(); err != nil {
			logger.Error("Failed to flush log", zap.Error(err))
		}
	}()
	if err != nil {
		return err
	}

	// set up proper shutdown of web server
	cctx, shutdown := context.WithCancel(context.Background())
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	defer func() {
		signal.Stop(quit)
		shutdown()
	}()

	go func() {
		select {
		case sig := <-quit:
			logger.Info("Graceful shutting triggered", zap.String("process_signal", sig.String()))
		case <-cctx.Done():
			logger.Info("Server context closed. Shutting down")
		}
		shutdown()
	}()

	if err := server.Start(cctx, conf, logger); err != nil {
		logger.Fatal("Failed to start server", zap.Error(err))
	}
	return nil
}
